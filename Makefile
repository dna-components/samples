ALL:
	@DIRS=`ls -d */`; \
	mkdir public/; \
	for dir in $$DIRS; do \
		echo "Installing $$dir"; \
		cd $$dir; \
		npm install; \
		cd ..; \
		cp -R $$dir "public/$$dir"; \
	done; \
	echo 'Done.'
