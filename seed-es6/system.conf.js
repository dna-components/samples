(function(System) {
    System.config({
        meta: {
            'dna/components': {
                format: 'global',
                exports: 'DNA',
            },
        },
        paths: {
            'dna/components': 'node_modules/dna-components/lib/dna.vdom-elements.js',
        },
    });
}(window.System));
