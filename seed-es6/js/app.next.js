import { register } from 'dna/components';
import { SeedComponent } from '../components/seed/seed-component.next.js';
// register the component
var Seed = register(SeedComponent);
document.body.appendChild(new Seed());
