(function (scope, angular) {
    var app = angular.module('app', ['dna-components']);
    app.run(['$rootScope', function ($rootScope) {
        $rootScope.owner = 'Chialabbers';
    }])
})(this, angular);
